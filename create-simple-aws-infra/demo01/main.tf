provider "aws" {
  region     = "eu-west-2"
}

resource "aws_key_pair" "ope_public_key" {
  key_name = "ope-public-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7/N8UQioiTFdOFjQiPhF7DdbuH8ZjR4saA9RxDosjL+uGZEbZAfrdOo7S5yL/E2tZv5eRmcpupziS1NwBJBHm1dI7yPrVt3FspzCwk2uKKKFyoBKFt0UCsqWwiGj5vVGcBTs3Qa2CYutG0yQYPyeSo4DUU1OJDLtz9YFtV17JXOZTsUVAnktma99ttN0a+i3mg0gknkLHvp5l1WpjXnUVeELIs0I0ja8quaC0E7eZivEaO/jvcD/zWJlzgAPBgfMLVV/3/BlRNXcG/8iOFV2hE3VwYB42IuAJmZEfLpO3yXEkOVXdEgdWBbis/IKin91+fF98nlm2Q7N6Vm1VxMux"
}

resource "aws_instance" "server01" {
  ami           = "ami-0596aab74a1ce3983"
  instance_type = "t2.micro"
  key_name = "ope-public-key"
  security_groups = ["${aws_security_group.servers_sg.name}"]
  #user_data = "${file("install-packages.sh")}"  

  tags = {
    Name = "server01"
    Purpose = "build-server, jenkins"
  }
}

resource "aws_instance" "server02" {
  ami           = "ami-0596aab74a1ce3983"
  instance_type = "t2.micro"
  key_name = "ope-public-key"
  security_groups = ["${aws_security_group.servers_sg.name}"]
  #user_data = "${file("install-packages.sh")}"

  tags = {
    Name = "server02"
    Purpose = "web-server, tomcat"
  }
}

# resource "aws_instance" "server03" {
#   ami           = "ami-0eb89db7593b5d434"
#   instance_type = "t2.micro"
#   key_name = "ope-public-key"
#   security_groups = ["${aws_security_group.servers_sg.name}"]
#   #user_data = "${file("install-packages.sh")}"

#   tags = {
#     Name = "server03"
#     Purpose = "testing.."
#   }
# }

resource "aws_security_group" "servers_sg" {
  name        = "servers_sg"
  description = "Allow traffic in and out of jenkins"

  ingress {
    description = "allow inbound port 8080"
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow inbound ssh port 22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "allow inbound docker swarm port 2376"
    from_port   = 2376
    to_port     = 2376
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "allow outbound all ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "servers-security-group"
  }
}

output "server01_public_ip" {
  value = "${aws_instance.server01.public_ip}"
}

output "server01_public_dns" {
  value = "${aws_instance.server01.public_dns}"
}

output "server02_public_ip" {
  value = "${aws_instance.server02.public_ip}"
}

output "server02_public_dns" {
  value = "${aws_instance.server02.public_dns}"
}

# output "server03_public_ip" {
#   value = "${aws_instance.server03.public_ip}"
# }

# output "server03_public_dns" {
#   value = "${aws_instance.server03.public_dns}"
# }
