#!/bin/sh

sudo yum remove java-1.7*
sudo yum install -y java-1.8*
sudo yum install -y git vim

# Creating maven directory under /opt
mkdir /opt/maven
cd /opt/maven
# downloading maven version 3.6.0
wget http://mirrors.estointernet.in/apache/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.tar.gz
tar -xvzf apache-maven-3.6.1-bin.tar.gz
mv apache-maven-3.6.1-bin.tar.gz maven
