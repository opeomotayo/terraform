provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "test-auto-rg" {
	name = "test-auto-rg"
	location = "UK SOUTH"
}
# Network Information
resource "azurerm_virtual_network" "test-auto-vnet" {
  name                = "test_auto_vnet"
  address_space       = ["10.0.0.0/8"]
  location            = "${azurerm_resource_group.test-auto-rg.location}"
  resource_group_name = "${azurerm_resource_group.test-auto-rg.name}"
}

resource "azurerm_subnet" "test-auto-snet" {
  name                 = "test_auto_vnet"
  resource_group_name  = "${azurerm_resource_group.test-auto-rg.name}"
  virtual_network_name = "${azurerm_virtual_network.test-auto-vnet.name}"
  address_prefix       = "10.12.10.0/24"
}

resource "azurerm_public_ip" "test-auto-pip" {
  name                 = "test_auto_pip"
  location             = "${azurerm_resource_group.test-auto-rg.location}"
  resource_group_name  = "${azurerm_resource_group.test-auto-rg.name}"
  allocation_method  = "Static"
}

resource "azurerm_network_security_group" "test-auto-nsg" {
  name                 = "test_auto_nsg"
  location             = "${azurerm_resource_group.test-auto-rg.location}"
  resource_group_name  = "${azurerm_resource_group.test-auto-rg.name}"

  security_rule {
    name                       =  "AllowRDPAccess"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefixes = ["109.147.22.83","78.41.240.74","109.148.118.72"]
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "AllowHTTPAccess"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefixes = ["109.147.22.83","78.41.240.74","109.148.118.72"]
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "test-auto-snsga" {
  subnet_id                 = azurerm_subnet.test-auto-snet.id
  network_security_group_id = azurerm_network_security_group.test-auto-nsg.id
}

resource "azurerm_network_interface" "test-auto-nic" {
  name                    = "test_auto_nic"
  location                = "${azurerm_resource_group.test-auto-rg.location}"
  resource_group_name     = "${azurerm_resource_group.test-auto-rg.name}"
  internal_dns_name_label = "test-auto-dns-label"

  ip_configuration {
    name                          = "test-auto-ip-conf"
    subnet_id                     = "${azurerm_subnet.test-auto-snet.id}"
    # private_ip_address            = "10.140.143.118"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.test-auto-pip.id}"
  }
}

resource "azurerm_virtual_machine" "test-auto-vm" {
  name                          = "test-auto-vm"
  location                      = "${azurerm_resource_group.test-auto-rg.location}"
  resource_group_name           = "${azurerm_resource_group.test-auto-rg.name}"
  network_interface_ids         = ["${azurerm_network_interface.test-auto-nic.id}"]
  vm_size                       = "Standard_D1_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "MicrosoftWindowsDesktop"
    offer     = "Windows-10"
    sku       = "rs5-pro"
    version   = "17763.1098.2003062342"
  }

  storage_os_disk {
    name              = "test-auto-os-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile_windows_config {
    enable_automatic_upgrades = false
    provision_vm_agent = true
  }

  os_profile {
    computer_name  = "test-auto"
    admin_username = "devops-admin"
    admin_password = "${var.admin-password}"
  }
}